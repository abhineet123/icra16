%!TEX ROOT = modularDecomposition.tex

\section{Introduction}
\label{introduction}

%An important component of any system(s) that relies on estimating the %trajectory of an object in a 
%continuous stream of images is 
%\emph{Object Tracking}. Applications like automated 
%surveillance \cite{user}, targeting systems \cite{target}, 
%robot manipulation \cite{nnic}, augmented reality \cite{arTracking}, image %stabilization 
%\cite{stabilization}, medical analysis \cite{medical} and many %others directly benefit from 
%advances in tracking research.
%	Tracking systems are an integral part of visual servoing algorithms. Servoing systems  \cite{servoing1, servoing2, servoing3} use fast trackers that have high convergence. Independent trackers are reported ~\cite{} to address this.
%Very little interest in paid to develop an integrated tracking system that has a wide range of tracking modalities. Hager et al. reported a system in XVision \cite{xvision} that introduced a full tracking framework including an efficient video pipeline. The number of trackers implemented were very limited and rather simple which are not robust to most videos. Visual servoing library VISP \cite{visp}

Since its inception, research in object tracking has focused on presenting new tracking algorithms to address specific challenges in a wide variety of application domains like surveillance \cite{user}, targeting systems \cite{target}, augmented reality \cite{arTracking} and medical analysis \cite{medical}. However, before
an algorithm can be adopted in a real life application, it needs to be extensively tested so that both limitations and advantages of the algorithm can be determined. Recent studies in tracking evaluation \cite{cvprBenchmark, vot, vot14} show increasing efforts to standardize this crucial process. Though such studies \cite{cvprBenchmark,vot, vot14,mubarakSummary} 
provide a global rank for each tracker, they provide little feedback to improve these trackers since they treat them as black boxes predicting the trajectory of the object. A more useful evaluation methodology would be to have empirical validation of the tracker's design or point out its shortcomings. 

Lately, such evaluations have been performed with on line learned trackers \cite{vot, vot14,mubarakSummary,cvprBenchmark} but are missing in the domain of registration based trackers. These are generally more precise than learning based trackers \cite{uofa} which makes them more suitable for applications in robotic manipulations and visual servoing. A detailed analysis, with a test framework in registration based tracking has never been attempted before to the best of our knowledge.

A tracking algorithm can be decomposed into three sub modules: appearance model,  search method and state space model. Figure \ref{flow} shows how these modules work together in a complete tracking system assuming there is no model update wherein the appearance model of the object is updated as tracking progresses.  

\begin{figure}[h]
	\begin{center}
	\includegraphics[height=1.8in]{modularNew.png}
	\caption{Modular breakdown of a tracking algorithm assuming there is no dynamic update to the appearance model. This shows how different components work, as formulated in Eq \ref{tracking}. The state vector $\pi$ parameterizes the pose of the object.}
	\label{flow}
	\end{center}
\end{figure}


When a geometric transform  $\mathbf{W}$ with parameters $\mathbf{p}=(p_1, p_2, ..., p_n)$ is applied to an image patch $\mathbf{x}$, the transformed patch is denoted by $\mathbf{x}'=\mathbf{W}(\mathbf{x}; \mathbf{p})$ and the corresponding pixel values become $\mathbf{I}(\mathbf{W}(\mathbf{x}; \mathbf{p}))$. Tracking
can then be formulated (Eq \ref{tracking}) as a state prediction problem where we need to predict the optimal transform parameters $\mathbf{p_t}$ for an image $\mathbf{I_t}$ that 
minimize the difference, measured by a suitable distance metric ($f$), between the target patch $\mathbf{I^*} = \mathbf{I_0}(\mathbf{W}(\mathbf{x};\mathbf{p_0}))$
and the warped image patch $\mathbf{I_t}(\mathbf{W}(\mathbf{x};\mathbf{p_t}))$. 


\begin{equation}
	\begin{aligned}
		\label{tracking}
		\mathbf{p_t} = \underset{\mathbf{p}} {\mathrm{argmin}} ~f(\mathbf{I_t}(\mathbf{W}(\mathbf{x}; \mathbf{p})), \mathbf{I^*})
	\end{aligned}
\end{equation}

The size of the state vector $\mathbf{p}$ determines how accurately the target is to be followed with greater values of $n$ usually leading to higher precision tracking. 
This is the state space for search. The algorithm that minimizes Eq \ref{tracking} is the search 
method. 




Most reported studies contribute in one of the three submodules and rarely explore the full extent of their contributions. For instance, Richa et. al \cite{hager} showed an improvement over the existing efficient second order minimization (ESM) \cite{esm} search method by using the sum of conditional variance (SCV) as the feature transform instead of raw pixel values. Similarly, Baker et. al \cite{ic} reported a compositional update scheme for the state parameters $\mathbf{p}$ instead of additive parameter update used in \cite{lk}, but never experimented with different pattern matching algorithms or showed experiments on large datasets of real videos. Conversely, Dame et. al \cite{MIIC} used mutual information (MI) while Scandaroli et. al \cite{ncc2012} used normalized cross correlation (NCC) as the similarity metric with an inverse compositional search method instead of the standard sum of squared differences (SSD) used in the original paper \cite{lk}. However, they did not test these appearance models with other search methods even though NCC was previously shown to be a good patch similarity metric in \cite{nccLK} when used with the standard Lucas Kanade type tracker.  

To avoid such fragmentation, a generic system would be more useful. Finding the optimal combination of methods for any tracking algorithm is a two step process. First, we need to figure out the sub module where the algorithm's main contribution lies, using, for instance, the method employed in \cite{cvprBenchmark}. Second, we need to enumerate 
all possible combinations for the other sub modules that are compatible with this algorithm since not all methods for different sub modules work with each other. 

The third sub module, namely state space model (SSM), represents any constraints that may be placed on the warping parameters to reduce the search space and make it easier for the search method to find the optimal warp. It may be noted that this sub module differs from the other two since it does not admit new methods in the conventional sense and may even be viewed as a part of the search method since the two are often closely intertwined in practical implementations.
However, though the state space models used in this work are limited to the standard hierarchy of geometric transformations with 2, 3, 4, 6 and 8 degrees of freedom (DOF) \cite{Hartley:2003:MVG:861369}, it is theoretically possible to come up with a novel constraint on the search space that can significantly decrease the search time while still producing sufficiently accurate results. The fact that such a constraint will be an important contribution in its own right justifies the use of state space models as a sub module in this work to motivate further research in this direction.

To summarize, following are the main contributions of this work:
\begin{itemize}
\item Empirically test different combinations of submodules leading to several interesting 
observations and insights that were missing in the original papers. Experiments are done using two large datasets having over 70,000 frames in all to ensure their statistical significance.

\item Compare formulations against popular Online Learned Trackers to validate their usability in precise tracking applications.

\item Provide an open source tracking framework using which all results can be reproduced and which, owing to its efficient C++ implementation, can also be used to address practical tracking requirements.
\end{itemize}





