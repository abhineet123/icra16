%!TEX ROOT = modularDecomposition.tex

\section{Experimental Results and Analysis}
\label{experiments}
%\begin{figure*}[t]
%\begin{center}
%\includegraphics[width=7.2in, height=3in]{sm_rscv_8}
%\caption{Success Rates for all search methods in the TMT dataset plotted against the error threshold.}
%\label{fig:sm_rscv_8_tmt}
%\end{center}
%\end{figure*}
\subsection{Dataset and Error Metric}
Two publicly available datasets have been used to analyze the trackers:
\begin{enumerate}
\item Tracking for Manipulation Tasks (\textbf{TMT}) dataset \cite{uofa} that contains videos of some common tasks performed at several speeds and under varying lighting conditions. It has 98 sequences in all with over 64000 frames. 
\item  Visual Tracking Dataset provided by \textbf{UCSB} \cite{gauglitz} that has 96 short sequences of different challenges in object tracking with a total of slightly under 7000 frames. The sequences here are more challenging but also rather artificial since they were created specifically to address various challenges rather than represent realistic scenarios.
\end{enumerate} 
Both these datasets have full pose (8 DOF) ground truth data which makes them suitable for evaluating high precision trackers that are the subject of this study. In addition, we use \textbf{Alignment Error} ($E_{AL}$) \cite{nnic,metaio} as metric to compare tracking result with the ground truth since it accounts for fine misalignments of pose better than other common measures like center location error and Jaccard index.

\subsection{Evaluation Measures}
We measure a tracker's overall accuracy through its \textbf{success rate} which is defined as the fraction of total frames where it tracks within an error ({$E_{AL}$) threshold of $t_p$ pixels. It is expressed as $\frac{|S|}{|F|}$, where $ S= \{f^{i} \in F : e_{AL}^{i} < t_p  \}$, $F$ is the set of all frames and $E_{AL}^{i}$ is the error in the $i^{th}$ frame ($f^{i}$). Since we have far too many sequences to present results for each, we instead choose to report the summary of results for each dataset by averaging the success rates over all sequences in that dataset. In addition we evaluate the success rate for several thresholds varying from 0 to 20 and study the resulting plot to get an overall idea of how precise a tracker is.
 
\subsection{Parameters Used}
All results have been generated using a fixed sampling resolution of \textbf{50x50} irrespective of the tracked object's size. In addition, a tracker was allowed to perform a maximum of \textbf{30 iterations} on each frame that were terminated when the L2 norm of the change in corners became less than \textbf{0.001}. For the NN tracker, we used \textbf{1000 samples} and a standard deviation of \textbf{0.05} for generating the random warps. The learning based trackers whose results are reported in Sec. \ref{res_ssm} were run using default settings provided by their respective authors. All tests were run on a 4 GHz Intel Core-i7 4790K machine with 16GB of RAM. No multi threading was used.

\subsection{Results}
The results presented in this section are organized into four sections. The first three sections correspond to the three sub modules and, in each of these, we present and analyze results comparing different methods for implementing the respective sub module while fixing the other sub modules to reasonable defaults. The fourth section presents speed comparisons for the different sub modules.
\label{results}
\subsubsection{Search Methods}
\label{res_sm}
\begin{figure}[h]
\begin{center}
\includegraphics[height=2.4in]{sm_rscv_8_tmt}
\caption{Success Rates for all search methods in the TMT dataset plotted against the error threshold.}
\label{fig:sm_rscv_8_tmt}
\end{center}
\end{figure}
\begin{figure}[h]
\begin{center}
\includegraphics[height=2.4in]{sm_rscv_8_ucsb}
\caption{Success Rates for all search methods in the UCSB dataset plotted against the error threshold.}
\label{fig:sm_rscv_8_ucsb}
\end{center}
\end{figure}
Fig. \ref{fig:sm_rscv_8_tmt} and \ref{fig:sm_rscv_8_ucsb} present the accuracy results for all the search methods on the TMT and UCSB datasets respectively using RSCV as the AM and homography as the SSM. Several interesting observations can be made here. Firstly, we see that the performance of the four variants of Lucas Kanade tracker are not similar for either dataset - FCLK is best followed by FALK and ICLK with IALK making up a distant fourth. This finding contradicts the equivalence between these variants that was reported in \cite{bakerMatthews} and justified using both theoretical analysis and experimental results. The latter, however, were only performed on synthetic images and even the former used several approximations. So, it is perhaps not surprising that this supposed equivalence does not hold under real world conditions. 

Secondly, we note that FCLK performs very similarly to ESM in TMT sequences and is even slightly better for higher thresholds but ESM is far better than FCLK in UCSB sequences. This indicates the superiority of ESM in more difficult scenarios, since, as stated earlier, UCSB sequences are much harder to track than TMT ones - a fact that is also borne out by the significantly lower success rates of all search methods in this dataset (note the different limits of y axis in Fig.  \ref{fig:sm_rscv_8_tmt} and \ref{fig:sm_rscv_8_ucsb}).

Thirdly, we see that NNIC is only slightly better than ICLK in TMT but much better in UCSB and is in fact the best of all search methods in this dataset. The reason for this is likely to be the same as that for the previous observation - the advantage of using NN to find a good starting point for the iterative search process is more marked in difficult situations especially those involving larger inter frame motions since the optimum is farther away from the tracker's position in the last frame.

The TMT dataset also annotates each frame with the challenges it presents. These can be used to figure out how well a tracker handles a specific challenge by examining the challenges associated with frames where it fails most often. We used these to generate success rates for all the search methods with respect to specific challenges and the results are reported in Fig. \ref{fig:tags_rscv_8}. It is rather difficult to draw interesting conclusions from these results since failure in a given frame due to a specific challenge also disqualifies a tracker from all subsequent frames even if they contain challenges that this tracker can handle well. Nevertheless a couple of observations can be made: we see that OC is the only challenge where ICLK performs better than both FCLK and FALK thus pointing to the advantage of using the gradient from the original image when the area under the current patch differs significantly from the template due to occlusion. We also conclude from the lowest relative scores in OC that this is the challenge that these trackers find it hardest to handle.

\begin{figure}[h]
\begin{center}
\includegraphics[width=3.4in]{tags_rscv_8}
\caption{Success rates for different search methods in the TMT dataset using RSCV/8DOF with respect to following challenges: \textbf{TR}: in plane translation \textbf{RO}: in plane rotation \textbf{PR}: perspective distortion \textbf{SR}: specular reflection \textbf{OC}: partial occlusion \textbf{TX}: changing texture \textbf{BL}: motion blur \textbf{SC}: scaling}
\label{fig:tags_rscv_8}
\end{center}
\end{figure}
\subsubsection{Appearance Models}
\label{res_am}
\begin{figure*}[t]
\begin{center}
\includegraphics[width=7in]{am_8}
\caption{Success Rates for all appearance models in both TMT and UCSB datasets plotted against the error threshold. Results for TMT are shown with \textbf{solid} lines while those for UCSB are shown with \textbf{dashed} lines}
\label{fig:am_8}
\end{center}
\end{figure*}
The success rates for the different AMs are shown in Fig. \ref{fig:am_8} for both datasets and the three best performing search methods. The first significant fact to notice is that NCC is either the best or only slightly behind the best in all 6 scenarios plotted here. This is rather contrary to expectations since SCV and RSCV are supposedly more robust against lighting changes due to their use of joint probability distributions while NCC, as we have implemented, is merely the L2 norm between the pixel values normalized to have zero mean and unit variance. We can also note that, while FCLK and ESM show large performance gap between the two datasets, NNIC is s much more consistent performer. 

We can see too that, though SCV performs well in TMT dataset for ESM and FCLK, it is the worst performer in UCSB and is in fact even worse than plain SSD. One reason for this can be that UCSB has  fewer sequences with illumination changes and more with high speed motions and the resultant blurring which probably is not an ideal scenario for SCV. At the same time, objects in UCSB have very distinct texture which suits SSD very well. Moving on to the NNIC plot, SCV seems to be either slightly better (TMT) or at par (UCSB) with SSD. This latter fact is probably because true SCV cannot be practically used with NN as it involves updating the \textit{template image} at each iteration. Since NN stores and searches amongst patches from the template image, it would have to update its index at each iteration to take into account the new template - a task that takes at least several seconds to complete for any reasonable number of samples and is thus unfeasible if real time performance is desired. Thus we are \textit{not} updating the index and as such the resulting AM is equivalent to SSD as far as NN is concerned though ICLK can still take advantage of the updated template which accounts for the slightly better performance in TMT. 

Two more interesting observations can be made from the NNIC plot. Firstly, although RSCV performs quite well in TMT, it is by far the worst performer in UCSB and in fact this is the only instance where this is so. Secondly, NCC performs much better than all other search methods in UCSB - a fact that is also reiterated when Fig. \ref{fig:sm_rscv_8_ucsb} is reproduced in Fig. \ref{fig:sm_ncc_8_ucsb} for NCC instead of RSCV. It is difficult to explain why NN should perform so much better with NCC in the more difficult dataset without going into internal workings of KD tree search (used by the FLANN library) which is outside the scope of this work. We can, however, suggest that this might be due to the better conditioning provided by the zero mean/unit variance pixel values used by NCC.
\begin{figure}[h]
\begin{center}
\includegraphics[height=2.3in]{sm_ncc_8_ucsb}
\caption{Success Rates for all search methods in the TMT dataset plotted against the error threshold using NCC AM and 8DOF SSM.}
\label{fig:sm_ncc_8_ucsb}
\end{center}
\end{figure}
\subsubsection{State Space Models}
\label{res_ssm}
\begin{figure*}
\begin{center}
\includegraphics[width=7in]{sm_rscv_2_gt2_speed}\\
\hspace{6em}(a)\hspace{15em}(b)\hspace{15em}(c)
\caption{Success Rates for 8 search methods using 2DOF SSM as well as for 3 learning based trackers on (a)TMT and (b)UCSB dataset. The former are shown with \textbf{solid} lines and the latter in \textbf{dashed} lines. 2DOF ground truth was used for all evaluations.}
\label{fig:sm_rscv_2_gt2_speed}
\end{center}
\end{figure*}
\begin{figure*}[t]
\begin{center}
\includegraphics[height=2.6in]{ssm_nesm_rscv2}
\caption{Success Rates for all state space models in both TMT and UCSB datasets plotted against the error threshold. Note that 8DOF homography has three different parameterizations whose plots overlap perfectly.} 
\label{fig:ssm_nesm_rscv}
\end{center}
\end{figure*}
\begin{figure*}[t]
\begin{center}
\includegraphics[height=2.5in]{speed_plots.pdf}
\caption{Speeds in FPS for the different submodules}
\label{fig:speed_plots}
\end{center}
\end{figure*}
The results presented in this section follow a slightly different format from the other two sections due to the difference in the motivations for using low DOF SSMs - the principle one being that reducing the dimensionality of the search space of warp parameters decreases the likelihood of the search process getting stuck in a local optimum and thus makes the tracker more robust. The other less important motivation is that lower DOF trackers tend to be faster since typically Jacobian are less expensive to compute.

In addition, limiting the DOF makes registration based trackers directly comparable to learning based trackers which too work in low DOF search spaces. As a result, in this section we also present results for three state of the art learning based trackers \cite{vot14} - discriminative scale space tracker (\textbf{DSST}), kernelized correlation filter tracker (\textbf{KCF}) and the tracking-learning-detection framework (\textbf{TLD}). We have used C++ implementations of all these trackers that are fully integrated into our framework. This not only makes it easy to reproduce the results presented here and but also makes it reasonable to compare the speeds of these trackers with the faster registration based trackers since slower speed is one if the main reasons why learning trackers are often not used in robotics applications. 

Lastly, in order to make the evaluations fair, we have used \textit{lower DOF ground truths} for all accuracy results in this section. These were generated by using least squares optimization to find, for each SSM, the warp parameters that, when applied to the initial bounding box, will produce a warped box whose alignment error ($E_{AL}$) with respect to the full 8 DOF ground truth is as small as it is possible to achieve given the constraints of that SSM. In most cases, the ground truth corners thus generated represent the best possible performance that can theoretically be achieved by any tracker that follows the constraints of that SSM. In some rare cases, however, the resulting corners can be quite unexpected so we also visually inspected all lower DOF corners and corrected any that appeared unreasonable.

Fig. \ref{fig:sm_rscv_2_gt2_speed} shows the performance of all search methods with 2DOF SSM in terms of both accuracy, evaluated against 2DOF ground truth, and speed, measured in number of frames processed by the tracker per second (FPS). The accuracy plots also include the results of another search method based on particle filter (\textbf{PF}).  As expected, all the learning based trackers have low success rates for low error thresholds since they are less precise in general \cite{vot}. Though their success rates start approaching those of the best registration based trackers at higher thresholds, they still remain lower with only one exception - DSST whose success rate in UCSB dataset increases sharply and quickly surpasses all other trackers. This is another instance where a more robust tracker performs significantly better relative to others on the more difficult UCSB dataset. Another interesting observation here is that IALK performs better than all other Lucas Kanade variants in UCSB which is contrary to its 8 DOF performance that is consistently far behind the others. It may be noted too that PF performs at par with the best registration trackers which ia unsurprising since we used 1000 particles for our tests and PF is known to perform well with low DOF when large number of particles are available.

The speed comparisons in Fig. \ref{fig:sm_rscv_2_gt2_speed}(c) clearly show the main reason why learning trackers are not suitable for  high speed tracking scenarios - they are 5 to 10 times slower than the registration trackers even though the latter use RSCV which is one of the slowest AMs. It may be noted that the speed of learning based trackers varied widely between sequences since it depends on the size of the initial bounding box unlike the registration trackers that use a fixed sampling resolution. However, the mean figures reported here do provide a good idea of the general performance that can be expected from these trackers. 

On comparing the speeds of different registration based trackers, we find the additive algorithms (IALK, FALK and AESM) to be significantly faster than their compositional counterparts (resp. ICLK, FCLK and ESM) which is a reversal of their usual behavior since they need to recompute the SSM Jacobian at each iteration and are thus slower. For 2 DOF motion, however, this Jacobian is fixed at identity for both additive and compositional formulations so the extra computation involved in computing the gradient of the warped image for compositional formulations becomes dominant. Though the speed plot does not include results for PF (as only a Python implementation is currently available), we can mention that it is far too slow for any practical tracking - it never went over 2 FPS in our tests. 

To conclude the analysis in this section, we tested the performance of different SSMs against each other and the results are reported in Fig. \ref{fig:ssm_nesm_rscv}. The accuracy plots for each SSM were generated by using the ground truth with the corresponding DOFs. As stated before, we were expecting lower DOF trackers to perform better here but this is not the case on the TMT dataset where higher DOF trackers clearly perform better. In UCSB dataset, however, this trend is reversed and we do see the expected increase in robustness with decrease in DOF. This indicates that the unexpected behavior in TMT is likely to be due to the relative ease of the tasks here that allow even the higher DOF trackers to perform quite well. 

It can also be noted that Fig.  \ref{fig:ssm_nesm_rscv} presents results for two other parameterizations of homography apart from the standard one - namely $\mathcal{SL}3$ (based on Lie Algebra) and corner based (using x,y coordinates of the four corners of the bounding box). We can see that all three parameterizations have exactly identical performance with their plots showing perfect overlap. This indicates that the theoretical justification given in \cite{esm} for parameterizing ESM with $\mathcal{SL}3$ has little practical significance. This, in turn, may suggest that the reason for ESM's superior performance over classical Lucas Kanade tracker has more to do with its use of the mean of the initial and current image gradients rather than with ESM providing a pseudo second order convergence (opposed to  Lucas Kanade's first order convergence) as proven in \cite{esm}. 

\subsubsection{Speed Comparisons}
\label{speeds}
The last results we present (Fig. \ref{fig:speed_plots}) are the comparative speeds of different methods for each sub module. The SM and SSM figures (left and middle) were generated by averaging over all sequences and all combinations of the other two submodules while the AM plot (right) was generated for 8DOF SSM by averaging over all SMs. To ensure accurate comparisons, we only consider the time taken by a tracker to process each frame and exclude the time spent in acquiring and preprocessing the images and in displaying the results. In the SM plot, we can observe that, as expected, ICLK is by far the fastest followed by IALK. Somewhat surprisingly though, FALK is slightly faster than FCLK and so is ESM. The latter can be explained by ESM needing fewer iterations to converge on average but the former is more difficult to explain given that additive formulations are expected to be slower since they require the SSM derivatives to be evaluated at the current parameter values leading to their re-computation at each iteration. It appears, however, that the extra cost of computing the \textit{gradient of warped image} - needed by FCLK - compared to the \textit{image gradient evaluated at warped locations} - needed by FALK - slightly outweighs this extra computation.

From the SSM plot, we see that lower DOFs are faster than higher DOFs with the exception of 3DOF which is slower than 4DOF since it uses a non linear parameterization (in terms of the rotation angle) which causes the sine and cosine of the angle to be computed at each iteration. We also note that corner based parameterization of homography is much slower than the other two since its Jacobian needs to be computed numerically. Finally, the AM plot shows us that, as expected, RSCV and SCV are both much slower than SSD and NCC even though the latter often performs at par or even better than these. This seems to indicate that NCC in general should be preferred over SCV/RSCV as the extra time can be used, for example, to utilize higher sampling resolutions or run more iterations per frame, both of which are more likely to improve performance.
